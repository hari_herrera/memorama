(function(window) {
  'use strict';
  window.AppConfig = {
    'deployEndpoint': '',
    'i18nPath': 'locales/',
    'componentsPath': './components/',
    'composerEndpoint': '/composerMocks/',
    'debug': false,
    'mocks': false,
    'coreCache': 'local',
    'routerLog': false
  };
})(window);