(function(window) {
  'use strict';
  window.AppEnvironment = {
    'config': {
      'deployEndpoint': '',
      'i18nPath': 'locales/',
      'componentsPath': './components/',
      'composerEndpoint': '/composerMocks/',
      'debug': false,
      'mocks': false,
      'coreCache': 'local',
      'routerLog': false
    }
  };
})(window);